﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ThirdPartyAuthentication.Startup))]
namespace ThirdPartyAuthentication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
